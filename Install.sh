#!/bin/bash

docker-compose down

# Install backend
sudo rm -rf express/node_modules
sudo rm -f express/package-lock.json
sudo npm install --prefix ./express

# Deploy docker-compose
docker-compose up --build -d
docker-compose logs -f
