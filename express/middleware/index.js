const AuthApi = require('./Auth.js');
const { validator, error } = require('bitrading-module-manager');

class Auth {
  constructor (host, port, user, pass, db) {
    this.db = new AuthApi(host, port, user, pass, db);
  }

  // POST: /auth/signin
  async post_signin (id, pw, email) {
    // return await this.db.User.register(id, pw, email);
  }

  // POST: /auth/login
  async post_login (id, pw) {
    try {
      try {
        id = validator('auth/id', id);
        pw = validator('auth/pw', pw);
      } catch (e) {
        throw error.new(error.codes.AUTH.LOGIN.PARAM.INVALID);
      }
      try {
        return await this.db.Auth.login(id, pw);
      } catch (e) {
        throw error.new(error.codes.AUTH.LOGIN.FAILED.NOMATCH);
      }
    } catch (e) {
      throw e;
    }
  }

  async post_isLogin (token) {
    try {
      return await this.db.Auth.isLogin(token);
    } catch (e) {
      throw e;
    }
  }
}

module.exports = Auth;
