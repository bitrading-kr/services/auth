const moment = require('moment-timezone');
class momentHelper {
  isExpired (dt) {
    return moment(dt) <= moment().utc();
  }
}
module.exports = new momentHelper();
