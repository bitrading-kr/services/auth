/**
 * @fileOverview 거래소 API
 * @name exchange.js
 * @author 2018 https://bitrading.kr
 * @copyright 2018 https://bitrading.kr
 * @created 2018-10-15
 */

const { TABLE } = require('./constants.js'),
      queryHelper = require('./queryHelper.js'),
      assertArgs = require('assert-args'),
      objectDefaults = require('object-defaults'),
      crypto = require('crypto'),
      moment = require('moment');
class Exchange {
  constructor (dbconnection) {
    this.db = dbconnection;
    this.helper = new queryHelper(this.db);
  }

  async add (...args) {
    let options = assertArgs(args, {
      name: 'string',
      symbol: 'string'
    });

    let params = {
      name: options.name,
      symbol: options.symbol
    };
    return await this.db(TABLE.EXCHANGE).insert(params);
  }

  async addMarket (...args) {
    let options = assertArgs(args, {
      name: 'string',
      symbol: 'string'
    });

    let params = {
      name: options.name,
      symbol: options.symbol
    };

    return await this.helper.insertUniq(['symbol'], TABLE.MARKET, params);
  }

  async addCoin (...args) {
    let options = assertArgs(args, {
      name: 'string',
      symbol: 'string'
    });

    let params = {
      name: options.name,
      symbol: options.symbol
    };

    return await this.helper.insertUniq(['symbol'], TABLE.COIN, params);
  }

  async combineMarket (...args) {
    let options = assertArgs(args, {
      exchange: 'string', // symbol
      market: 'string'
    });

    let exchangeId = await this.getId(options.exchange),
        marketId = await this.getMarketId(options.market);

    // Errors
    if (exchangeId === undefined) throw new Error(`'${options.exchange}'에 해당하는 id가 존재하지 않습니다.`);
    if (marketId === undefined) throw new Error(`'${options.market}'에 해당하는 id가 존재하지 않습니다.`);

    let params = {
      'ExchangeID': exchangeId,
      'MarketID': marketId,
    };
    let res = await this.helper.selectWhere('ExchangeID', TABLE.EXCHANGE_MARKET, params);
    if (res !== undefined) return false;
    return await this.helper.insert(TABLE.EXCHANGE_MARKET, params);
  }

  async combineCoin (...args) {
    let options = assertArgs(args, {
      exchange: 'string', // symbol
      coin: 'string'
    });

    let exchangeId = await this.getId(options.exchange),
        coinId = await this.getCoinId(options.coin);

    // Errors
    if (exchangeId === undefined) throw new Error(`'${options.exchange}'에 해당하는 id가 존재하지 않습니다.`);
    if (coinId === undefined) throw new Error(`'${options.coin}'에 해당하는 id가 존재하지 않습니다.`);

    let params = {
      'ExchangeID': exchangeId,
      'CoinID': coinId,
    };
    let res = await this.helper.selectWhere('ExchangeID', TABLE.EXCHANGE_COIN, params);
    if (res !== undefined) return false;
    return await this.helper.insert(TABLE.EXCHANGE_COIN, params);
  }

  async getAll () {
    return await this.db(TABLE.EXCHANGE).select(['name', 'symbol']);
  }

  async getMarketAll (exchangeSymbol) {
    if (exchangeSymbol) {
      return await this.db.select(['name', 'symbol']).from(TABLE.MARKET).innerJoin(
        this.db.select('MarketID').from(TABLE.EXCHANGE_MARKET).where(
          'ExchangeID', this.db.select('id').from(TABLE.EXCHANGE).where('symbol', exchangeSymbol)).as('_A')
        , '_A.MarketID', `${TABLE.MARKET}.id`);
    } else {
      return await this.db(TABLE.MARKET).select(['name', 'symbol']);
    }
  }

  async getCoinAll (exchangeSymbol) {
    if (exchangeSymbol) {
      return await this.db.select(['name', 'symbol']).from(TABLE.COIN).innerJoin(
        this.db.select('CoinID').from(TABLE.EXCHANGE_COIN).where(
          'ExchangeID', this.db.select('id').from(TABLE.EXCHANGE).where('symbol', exchangeSymbol)).as('_A')
        , '_A.CoinID', `${TABLE.COIN}.id`);
    } else {
      return await this.db(TABLE.COIN).select(['name', 'symbol']);
    }
  }

  async getId (...args) {
    let options = assertArgs(args, {
      'symbol': 'string',
    });
    return await this.helper.selectId(TABLE.EXCHANGE, 'symbol', options.symbol);
  }

  async getMarketId (...args) {
    let options = assertArgs(args, {
      'symbol': 'string',
    });
    return await this.helper.selectId(TABLE.MARKET, 'symbol', options.symbol);
  }

  async getCoinId (...args) {
    let options = assertArgs(args, {
      'symbol': 'string',
    });
    return await this.helper.selectId(TABLE.COIN, 'symbol', options.symbol);
  }

  async getName (...args) {
    let options = assertArgs(args, {
      'symbol': 'string',
    });
  }

  async getMarketName (...args) {
    let options = assertArgs(args, {
      'symbol': 'string',
    });
  }

  async getCoinName (...args) {
    let options = assertArgs(args, {
      'symbol': 'string',
    });
  }

}

module.exports = Exchange;
