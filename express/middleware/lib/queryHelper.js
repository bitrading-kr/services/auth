"use strict";
const _ = require('lodash');

class queryHelper {
  constructor (dbConnection) {
    this.db = dbConnection;
  }

  // TODO
  async insertUniq (uniqCols, table, params) {
    let res = await this.db(table).where(builder => {
      uniqCols.forEach(col => {
        builder.orWhere(col, params[col]);
      });
    });
    if (!Array.isArray(res) || res.length != 0) return false;

    let insertRes = await this.db(table).insert(params);
    return insertRes;
  }

  async insert (table, params) {
    let insertRes = await this.db(table).insert(params);
    return insertRes;
  }

  async update (table, params, where) {
    let updateRes = await this.db(table).where(where).update(params);
    return updateRes;
  }

  async selectId (from, whereCol, whereValue, whereMatch) {
    let res = await this.selectWhere('id', from, whereCol, whereValue, whereMatch);
    if (res === undefined) return res;
    return res[0].id;
  }

  // Constraint errors, return value or undefined
  async selectWhere (columns, from, whereCol, whereValue, whereMatch='=') {
    let res;
    try {
      if (_.isPlainObject(whereCol)) {
        res = await this.db.select(columns).from(from).where(whereCol);
      } else {
        res = await this.db.select(columns).from(from).where(whereCol, whereMatch, whereValue);
      }
    } catch (err) {
      console.log(err);
      return undefined;
    }

    if (!Array.isArray(res)) {
      return undefined;
    }

    if (res.length == 0) {
      return undefined;
    }

    return res;
  }
}

module.exports = queryHelper;
