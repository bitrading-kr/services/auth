const TABLE = {
  'USER': 'bt_user',
  'GROUP': 'bt_group',
  'LICENSE': 'bt_license',
  'MARKET': 'bt_market',
  'COIN': 'bt_coin',
  'EXCHANGE': 'bt_exchange',
  'UUID': 'bt_uuid',
  'USER_MEMBERSHIP': 'bt_user_membership',
  'USER_ACCESS': 'bt_user_access',
  'USER_LICENSE': 'bt_user_license',
  'USER_API': 'bt_user_api',
  'USER_GROUP': 'bt_user_group',
  'GROUP_MEMBERSHIP': 'bt_group_membership',
  'GROUP_INVITATION_CODE': 'bt_group_invitation_code',
  'EXCHANGE_MARKET': 'bt_exchange_market',
  'EXCHANGE_COIN': 'bt_exchange_coin',
  'ORDER': 'bt_order',
  'TRADING': 'bt_trading',
  'ENTRY_STRATEGY': 'bt_entry_strategy',
  'TARGET_STRATEGY': 'bt_target_strategy',
  'TRAILING_STOP_STRATEGY': 'bt_trailing_stop_strategy'
};
module.exports = {
  TABLE: TABLE
};
