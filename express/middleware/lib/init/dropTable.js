class Drop {
  constructor (dbConnection) {
    this.db = dbConnection;
  }

  _dropTable (tableName) {
    return new Promise((resolve, reject) => {
      this.db.schema.dropTableIfExists(tableName).then(res => resolve(res)).catch(err => reject(err));
    });
  }

  user () {
    return this._dropTable('bt_user');
  }

  group () {
    return this._dropTable('bt_group');
  }

  license () {
    return this._dropTable('bt_license');
  }

  market () {
    return this._dropTable('bt_market');
  }

  coin () {
    return this._dropTable('bt_coin');
  }

  exchange () {
    return this._dropTable('bt_exchange');
  }

  uuid () {
    return this._dropTable('bt_uuid');
  }

  user_membership () {
    return this._dropTable('bt_user_membership');
  }

  user_access () {
    return this._dropTable('bt_user_access');
  }

  user_license () {
    return this._dropTable('bt_user_license');
  }

  user_api () {
    return this._dropTable('bt_user_api');
  }

  user_group () {
    return this._dropTable('bt_user_group');
  }

  group_membership () {
    return this._dropTable('bt_group_membership');
  }

  group_invitation_code () {
    return this._dropTable('bt_group_invitation_code');
  }

  exchange_market () {
    return this._dropTable('bt_exchange_market');
  }

  exchange_coin () {
    return this._dropTable('bt_exchange_coin');
  }

  order () {
    return this._dropTable('bt_order');
  }

  trading () {
    return this._dropTable('bt_trading');
  }

  entry_strategy () {
    return this._dropTable('bt_entry_strategy');
  }

  target_strategy () {
    return this._dropTable('bt_target_strategy');
  }

  trailing_stop_strategy () {
    return this._dropTable('bt_trailing_stop_strategy');
  }

}

module.exports = Drop;
