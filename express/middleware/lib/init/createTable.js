class CreateTable {
  constructor (dbConnection) {
    this.db = dbConnection;
  }

  _createTable (tableName, func) {
    return new Promise((resolve, reject) => {
      this.db.schema.hasTable(tableName).then(exists => {
        if (!exists) {
          this.db.schema.createTable(tableName, func)
            .then(res => resolve(res))
            .catch(err => reject(err));
        } else {
          resolve(true);
        }
      }).catch(err => reject(err));
    });
  }

  user () {
    return this._createTable('bt_user', t => {
      t.increments('id').primary().notNullable();
      t.string('login_id').notNullable().unique();
      t.string('login_pw').notNullable();
    });
  }

  group () {
    return this._createTable('bt_group', t => {
      t.increments('id').primary().notNullable();
      t.string('name').notNullable().unique();
      t.string('description');
    });
  }

  license () {
    return this._createTable('bt_license', t => {
      t.increments('id').primary().notNullable();
      t.string('license_key').notNullable().unique();
      t.integer('type').unsigned().defaultTo(0);
      t.timestamp('expired_date').notNullable().defaultTo(this.db.fn.now());
    });
  }

  market () {
    return this._createTable('bt_market', t => {
      t.increments('id').primary().notNullable();
      t.string('name').notNullable();
      t.string('symbol').notNullable().unique();
    });
  }

  coin () {
    return this._createTable('bt_coin', t => {
      t.increments('id').primary().notNullable();
      t.string('name').notNullable();
      t.string('symbol').notNullable().unique();
    });
  }

  exchange () {
    return this._createTable('bt_exchange', t => {
      t.increments('id').primary().notNullable();
      t.string('name').notNullable();
      t.string('symbol').notNullable().unique();
    });
  }

  uuid () {
    return this._createTable('bt_uuid', t => {
      t.increments('id').primary().notNullable();
      t.string('uuid').notNullable();
      t.string('side').notNullable();
      t.string('ord_type').notNullable();
      t.float('price').notNullable();
      t.float('avg_price').notNullable();
      t.string('state').notNullable();
      t.string('market').notNullable();
      t.timestamp('created_at').notNullable().defaultTo(this.db.fn.now());
      t.float('volume').notNullable();
      t.float('remaining_volume').notNullable();
      t.float('reserved_fee').notNullable();
      t.float('remaining_fee').notNullable();
      t.float('locked').notNullable();
      t.float('executed_volume').notNullable();
      t.integer('trade_count').notNullable();
    });
  }

  user_membership () {
    return this._createTable('bt_user_membership', t => {
      t.integer('UID').unsigned().notNullable();
      t.string('email').notNullable();
      t.timestamp('registered').notNullable().defaultTo(this.db.fn.now());
      t.integer('status').unsigned().notNullable().defaultTo(0);

      t.foreign('UID').references('bt_user.id').onUpdate('CASCADE').onDelete('CASCADE');
    });
  }

  user_access () {
    return this._createTable('bt_user_access', t => {
      t.integer('UID').unsigned().notNullable().unique();
      t.string('access_key').notNullable();
      t.string('secret_key').notNullable();
      t.timestamp('expired_date').notNullable().defaultTo(this.db.fn.now());
      t.foreign('UID').references('bt_user.id').onUpdate('CASCADE').onDelete('CASCADE');
    });
  }

  user_license () {
    return this._createTable('bt_user_license', t => {
      t.integer('UID').unsigned().notNullable();
      t.integer('LicenseID').unsigned().notNullable();

      t.foreign('UID').references('bt_user.id').onUpdate('CASCADE').onDelete('CASCADE');
      t.foreign('LicenseID').references('bt_license.id').onUpdate('CASCADE').onDelete('CASCADE');
    });
  }

  user_api () {
    return this._createTable('bt_user_api', t => {
      t.increments('id').primary().notNullable();
      t.integer('UID').unsigned().notNullable();
      t.integer('ExchangeID').unsigned().notNullable();
      t.string('access_key').defaultTo(null);
      t.string('secret_key').defaultTo(null);

      t.foreign('UID').references('bt_user.id').onUpdate('CASCADE').onDelete('CASCADE');
      t.foreign('ExchangeID').references('bt_exchange.id').onUpdate('CASCADE').onDelete('CASCADE');
    });
  }

  user_group () {
    return this._createTable('bt_user_group', t => {
      t.integer('UID').unsigned().notNullable();
      t.integer('GID').unsigned().notNullable();
      t.integer('level').unsigned().defaultTo(0);

      t.foreign('UID').references('bt_user.id').onUpdate('CASCADE').onDelete('CASCADE');
      t.foreign('GID').references('bt_group.id').onUpdate('CASCADE').onDelete('CASCADE');
    });
  }

  group_membership () {
    return this._createTable('bt_group_membership', t => {
      t.integer('GID').unsigned().notNullable();
      t.string('email').notNullable();
      t.string('homepage');
      t.string('telegram');
      t.timestamp('registered').notNullable().defaultTo(this.db.fn.now());

      t.foreign('GID').references('bt_group.id').onUpdate('CASCADE').onDelete('CASCADE');
    });
  }

  group_invitation_code () {
    return this._createTable('bt_group_invitation_code', t => {
      t.integer('GID').unsigned().notNullable();
      t.string('code').notNullable();
      t.timestamp('expired_date').notNullable().defaultTo(this.db.fn.now());

      t.foreign('GID').references('bt_group.id').onUpdate('CASCADE').onDelete('CASCADE');
    });
  }

  exchange_market () {
    return this._createTable('bt_exchange_market', t => {
      t.integer('ExchangeID').unsigned().notNullable();
      t.integer('MarketID').unsigned().notNullable();

      t.foreign('ExchangeID').references('bt_exchange.id').onUpdate('CASCADE').onDelete('CASCADE');
      t.foreign('MarketID').references('bt_market.id').onUpdate('CASCADE');
    });
  }

  exchange_coin () {
    return this._createTable('bt_exchange_coin', t => {
      t.integer('ExchangeID').unsigned().notNullable();
      t.integer('CoinID').unsigned().notNullable();

      t.foreign('ExchangeID').references('bt_exchange.id').onUpdate('CASCADE').onDelete('CASCADE');
      t.foreign('CoinID').references('bt_coin.id').onUpdate('CASCADE');
    });
  }

  order () {
    return this._createTable('bt_order', t => {
      t.increments('id').primary().notNullable();
      t.integer('UID').unsigned().notNullable();
      t.integer('UAPI').unsigned();
      t.integer('SourceID').unsigned();
      t.string('title').defaultTo('');
      t.boolean('status').defaultTo(true);
      t.timestamp('registered').notNullable().defaultTo(this.db.fn.now());
      t.integer('ExchangeID').unsigned();
      t.integer('MarketID').unsigned();
      t.integer('CoinID').unsigned();

      t.foreign('UID').references('bt_user.id').onUpdate('CASCADE').onDelete('CASCADE');
      t.foreign('UAPI').references('bt_user_api.id').onUpdate('CASCADE').onDelete('SET NULL');
      t.foreign('ExchangeID').references('bt_exchange.id').onUpdate('CASCADE').onDelete('SET NULL');
      t.foreign('MarketID').references('bt_market.id').onUpdate('CASCADE').onDelete('SET NULL');
      t.foreign('CoinID').references('bt_coin.id').onUpdate('CASCADE').onDelete('SET NULL');
    });
  }

  trading () {
    return this._createTable('bt_trading', t => {
      t.increments('id').primary().notNullable();
      t.integer('OrderID').unsigned().notNullable();
      t.integer('UuidID').unsigned().notNullable();
      t.timestamp('registered').notNullable().defaultTo(this.db.fn.now());
      t.timestamp('updated').notNullable();

      t.foreign('OrderID').references('bt_order.id').onUpdate('CASCADE').onDelete('CASCADE');
      t.foreign('UuidID').references('bt_uuid.id').onUpdate('CASCADE').onDelete('CASCADE');
    });
  }

  entry_strategy () {
    return this._createTable('bt_entry_strategy', t => {
      t.integer('OrderID').unsigned().notNullable();
      t.float('price').notNullable();
      t.float('quantity').notNullable();
      t.integer('TradingID').unsigned();

      t.foreign('OrderID').references('bt_order.id').onUpdate('CASCADE').onDelete('CASCADE');
      t.foreign('TradingID').references('bt_trading.id').onUpdate('CASCADE').onDelete('SET NULL');
    });
  }

  target_strategy () {
    return this._createTable('bt_target_strategy', t => {
      t.integer('OrderID').unsigned().notNullable();
      t.float('price').notNullable();
      t.float('quantity').notNullable();
      t.integer('TradingID').unsigned();

      t.foreign('OrderID').references('bt_order.id').onUpdate('CASCADE').onDelete('CASCADE');
      t.foreign('TradingID').references('bt_trading.id').onUpdate('CASCADE').onDelete('SET NULL');
    });
  }

  trailing_stop_strategy () {
    return this._createTable('bt_trailing_stop_strategy', t => {
      t.integer('OrderID').unsigned().notNullable();
      t.float('stop_limit').notNullable();
      t.float('ratio').notNullable();
      t.float('price').notNullable();
      t.float('quantity').notNullable();
      t.integer('TradingID').unsigned();

      t.foreign('OrderID').references('bt_order.id').onUpdate('CASCADE').onDelete('CASCADE');
      t.foreign('TradingID').references('bt_trading.id').onUpdate('CASCADE').onDelete('SET NULL');
    });
  }

}

module.exports = CreateTable;
