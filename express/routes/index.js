const express = require('express'),
      router = express.Router(),
      axios = require('axios'),
      _ = require('lodash');

// require middlewares
const Auth = require('../middleware/index.js');
const auth = new Auth(process.env.DB_HOST, process.env.DB_PORT, process.env.DB_USER, process.env.DB_PASSWD, process.env.DB_DB);

const HEADERS = {
  auth_token: 'user-auth-token'
};

function errorHandler (res, err) {
  return res.status(500).send(err).end();
}
/* GET home page. */
router.get('/', function(req, res, next) {
  res.send({"HI": "HEY"});
});

router.get('/login', function(req, res, next) {
  res.send({"status": "login"});
});

router.get('/resources', function(req, res, next) {
  res.send({"status": "login"});
});

/*
 * Auth
 */
router.post('/auth/login', function(req, res, next) {
  auth.post_login(_.get(req, 'body.id'), _.get(req, 'body.password'))
    .then(result => res.send(result).end())
    .catch(err => errorHandler(res, err));
});

router.get('/auth/login', function(req, res, next) {
  console.log(req.headers);
  auth.post_isLogin(_.get(req.headers, HEADERS.auth_token))
    .then(result => res.send(result).end())
    .catch(err => errorHandler(res, err));
});

/*
 * Users
 */
router.post('/auth/signup', function(req, res, next) {
  // console.log(req);
  res.send(req);
  // api.Resources.get_exchanges()
  //   .then(result => res.send(result).end())
  //   .catch(err => res.status(500).send(err));
});

router.get('/test', async function(req, res, next) {
  try {
    let a = await axios.post('http://localhost:3000/auth/login', {
      id: '',
      password: ''
    });
    console.log(`hi`);
    let token = a.data;
    let login = await axios.get('http://localhost:3000/auth/login', {
      headers: {
        user_auth_token: token
      }
    });
    res.send(`Token: ${token}\nLogin: ${login.data}`).end();
  } catch (e) {
    res.send(e.message).end();
  }
});

module.exports = router;
